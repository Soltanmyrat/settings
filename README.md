# settings
The project was created to store and update personalized settings.
---
___
File [Plugins.txt](https://gitlab.com/Soltanmyrat/settings/-/blob/main/vscode/Plugins.txt) - list of plugins in use:
---
1. Plugins for VSCode
     + CodeSnap
     + EditorConfig for VS Code
     + File Utils
     + Image preview
     + Live Server
     + Material Icon Theme
     + Trailing Spaces
2. Plugins for README
     + Markdown All in One
___
File [settings.json](https://gitlab.com/Soltanmyrat/settings/-/blob/main/vscode/settings.json) - for personal settings for VSCode.
---
___
The files are available both for general use and as a template for your own settings.
---
___
Author: Meredov Soltanmyrat.
---
